﻿using System;
using System.Collections.Generic;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.Unmanaged
{
    public static class MemoryScanner
    {
        public static unsafe List<IntPtr> Search(byte[] value, byte[] mask)
        {
            if (value.Length != mask.Length)
                throw new ArgumentException($"{nameof(value)} and {nameof(mask)} must have the same {nameof(value.Length)}.");
            var patternLength = value.Length;

            var matches = new List<IntPtr>();
            var mbi = new MemoryBasicInformation();
            while (Kernel32.VirtualQuery(mbi.BaseAddress + mbi.RegionSize, ref mbi) > 0)
            {
                Page old;
                if (Kernel32.VirtualProtect(mbi.BaseAddress, mbi.RegionSize, Page.ExecuteReadWrite, out old))
                {
                    var data = (byte*)mbi.BaseAddress;
                    var size = mbi.RegionSize - patternLength;

                    for (var i = 0; i < size; i++)
                    {
                        var match = true;
                        for (var j = 0; j < patternLength; j++)
                        {
                            if (mask[j] == 0x00 || data[i + j] == value[j])
                                continue;
                            match = false;
                            break;
                        }
                        if (match)
                            matches.Add(new IntPtr(&data[i]));
                    }
                }
                Kernel32.VirtualProtect(mbi.BaseAddress, mbi.RegionSize, old);
            }
            return matches;
        }

        public static List<IntPtr> Search(Pattern pattern) => Search(pattern.Value, pattern.Mask);
    }
}
