﻿using System;
using System.Globalization;

namespace MindWorX.Unmanaged
{
    public class Pattern
    {
        /// <summary>
        /// Creates a pattern.
        /// </summary>
        /// <param name="pattern">Use ? or ?? for parts where the value doesn't matter.</param>
        /// <returns></returns>
        public static Pattern FromString(string pattern)
        {
            pattern = pattern.Replace("??", "?");
            var parts = pattern.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var value = new byte[parts.Length];
            var mask = new byte[parts.Length];
            for (var i = 0; i < parts.Length; i++)
            {
                switch (parts[i])
                {
                    case "?":
                        value[i] = 0x00;
                        mask[i] = 0x00;
                        break;
                    default:
                        value[i] = Byte.Parse(parts[i], NumberStyles.AllowHexSpecifier);
                        mask[i] = 0xFF;
                        break;
                }
            }
            return new Pattern(value, mask);
        }


        public Pattern(byte[] value, byte[] mask)
        {
            if (value.Length != mask.Length)
                throw new ArgumentException($"{nameof(value)} and {nameof(mask)} must have the same {nameof(value.Length)}.");
            this.Value = value;
            this.Mask = mask;
            this.Length = this.Value.Length;
        }

        public byte[] Value { get; }

        public byte[] Mask { get; }

        public int Length { get; }
    }
}
