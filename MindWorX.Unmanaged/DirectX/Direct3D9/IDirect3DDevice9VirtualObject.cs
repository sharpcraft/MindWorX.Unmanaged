﻿using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.DirectX.Direct3D9
{
    [StructLayout(LayoutKind.Sequential)]
    // ReSharper disable once InconsistentNaming
    public unsafe struct IDirect3DDevice9VirtualObject
    {
        public IDirect3DDevice9VirtualTable* MethodTable;
    }
}
