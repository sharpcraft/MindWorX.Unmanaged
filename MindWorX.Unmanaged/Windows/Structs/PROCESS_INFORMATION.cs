﻿// ReSharper disable InconsistentNaming

using System;

namespace MindWorX.Unmanaged.Windows
{
    public struct PROCESS_INFORMATION
    {
        public IntPtr hProcess;
        public IntPtr hThread;
        public uint dwProcessId;
        public uint dwThreadId;
    }
}
