﻿using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageFileHeader
    {
        public ImageFileMachine Machine;

        public short NumberOfSections;

        public int TimeDateStamp;

        public int PointerToSymbolTable;

        public int NumberOfSymbols;

        public short SizeOfOptionalHeader;

        public short Characteristics;
    }
}
