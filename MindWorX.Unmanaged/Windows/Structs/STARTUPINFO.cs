﻿// ReSharper disable InconsistentNaming

using System;

namespace MindWorX.Unmanaged.Windows
{
    public unsafe struct STARTUPINFO
    {
        public int cb;
        public void* lpReserved; // char*
        public void* lpDesktop; // char*
        public void* lpTitle; // char*
        public int dwX;
        public int dwY;
        public int dwXSize;
        public int dwYSize;
        public int dwXCountChars;
        public int dwYCountChars;
        public int dwFillAttribute;
        public int dwFlags;
        public short wShowWindow;
        public short cbReserved2;
        public byte* lpReserved2;
        public IntPtr hStdInput;
        public IntPtr hStdOutput;
        public IntPtr hStdError;
    }
}
