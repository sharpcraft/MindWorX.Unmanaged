﻿using System.Runtime.InteropServices;

namespace MindWorX.Unmanaged.Windows
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct ImageNTHeader
    {
        public int Signature;

        public ImageFileHeader FileHeader;

        public ImageOptionalHeader OptionalHeader;

        public int Size => sizeof(int) + sizeof(ImageFileHeader) + this.FileHeader.SizeOfOptionalHeader;
    }
}
