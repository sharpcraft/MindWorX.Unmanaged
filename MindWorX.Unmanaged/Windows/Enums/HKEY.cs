﻿// ReSharper disable InconsistentNaming

using System;

namespace MindWorX.Unmanaged.Windows
{
    public static class HKEY
    {
        public static UIntPtr CLASSES_ROOT = new UIntPtr(0x80000000u);

        public static UIntPtr CURRENT_USER = new UIntPtr(0x80000001u);

        public static UIntPtr LOCAL_MACHINE = new UIntPtr(0x80000002u);

        public static UIntPtr USERS = new UIntPtr(0x80000003u);

        public static UIntPtr HKEY_PERFORMANCE_DATA = new UIntPtr(0x80000004u);

        public static UIntPtr CURRENT_CONFIG = new UIntPtr(0x80000005u);

        public static UIntPtr DYN_DATA = new UIntPtr(0x80000006u);
    }
}
