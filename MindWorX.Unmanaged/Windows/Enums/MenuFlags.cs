﻿using System;

namespace MindWorX.Unmanaged.Windows
{
    [Flags]
    public enum MenuFlags : uint
    {
        Enabled = 0x00000000,
        String = 0x00000000,
        Unchecked = 0x00000000,

        Grayed = 0x00000001,
        Disabled = 0x00000002,
        Bitmap = 0x00000004,
        Checked = 0x00000008,
        Popup = 0x00000010,
        MenuBarBreak = 0x00000020,
        MenuBreak = 0x00000040,
        OwnerDraw = 0x00000100,
        Separator = 0x00000800,
    }
}
