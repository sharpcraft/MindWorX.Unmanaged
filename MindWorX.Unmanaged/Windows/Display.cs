﻿using System.Collections.Generic;

namespace MindWorX.Unmanaged.Windows
{
    public static class Display
    {
        public static IEnumerable<DEVMODE> Settings
        {
            get
            {
                var devmode = new DEVMODE();
                var i = 0;
                while (User32.EnumDisplaySettings(null, i++, ref devmode))
                    yield return devmode;
            }
        }
    }
}
